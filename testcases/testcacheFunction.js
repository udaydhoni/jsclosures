let cacheFunction = require('../cacheFunction.js');

function returnWhatISay (x) {
    return x;
}

let invokeCallBackWith = cacheFunction (returnWhatISay);

console.log(invokeCallBackWith('Hello'));
console.log(invokeCallBackWith('Uday'));
console.log(invokeCallBackWith('Hello'));
console.log(invokeCallBackWith('working'));

