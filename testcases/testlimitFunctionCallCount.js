let limitFunctionCallCount = require('../limitFunctionCallCount');

let limit = 3;

function returnStatement () {

    return ("I love 3 idiots and I'll say it only 3 times");
}

let invokeCallBack = limitFunctionCallCount (returnStatement,limit);

console.log(invokeCallBack());
console.log(invokeCallBack());
console.log(invokeCallBack());
console.log(invokeCallBack());
