function limitFunctionCallCount (cb,n) {
    var count = 0;                                      // This is to keep track of number of times cb invoked.
    return function invokeCb(){                         //Here we wrote function which helps to invoke callBack
        if (count < n) {                                // when the invokeCb is called it checks how many times the cb is invoked if it is less than n it would invoke again
            count +=1;
            return cb();                                  // After invoking it increases our count
        }
        else {
            return "You've exceeded the limit";
        }

    }
}

module.exports = limitFunctionCallCount;