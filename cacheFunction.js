function cacheFunction (cb) {
    let cache = {};
    return function invokeCb (x) {
        if (cache.hasOwnProperty(x)) {
            console.log(cache);
            return cache[x];
        } else {
            cache[x] = cb(x);
            console.log(cache);
            return cb(x);
        }
    }
}

module.exports = cacheFunction;

