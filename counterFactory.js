function counterFactory (counter) {

    return {
        increment: function () {
            counter++;
            return counter;
        },
        decrement: function () {
            counter--;
            return counter;
        }
    }
}

module.exports = counterFactory;
